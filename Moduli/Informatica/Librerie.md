# Librerie

## Concetti

- Libreria vs. Framework
- Classi e funzioni
- API
- Documentazione
- Contribuire

## Spiegazione

In programmazione, le librerie sono un modo rapido ed efficace per aggiungere funzionalità avanzate al proprio codice sorgente, senza dover reinventare la ruota ad ogni progetto.

Nella totalità dei progetti di un programmatore ci sarà sempre una parte non scritta dallo stesso (molto spesso quel programmatore sei te stesso del passato), ma che fa affidamento a del codice esterno per facilitare un problema specifico. Molto spesso è possibile trovare diverse librerie che hanno affrontato e risolto lo stesso problema, ognuna con i suoi pro ed i suoi contro, e vien da se quindi che saper scegliere, conoscere ed utilizzare delle librerie esterne è tanto importante quanto saper creare delle soluzioni autonomamente (nel caso non esistessero ancora librerie per il proprio scopo).

Per via del repentino avanzamento avvenuto nel mondo del _web development_ negli ultimi 20 anni, nel mondo dell'informatica è nata molta confusione nei termini e nelle caratteristiche riguardanti librerie, _framework_, _IDE_, _API_ e tutto quello che riguarda l'inclusione di risorse esterne all'interno del proprio progetto. Per chiarirsi le idee, è possibile partire dalla seguente distinzione:

- Una libreria si adatta al tuo progetto ed alla sua architettura
- Il tuo progetto e la sua architettura si formano intorno ad un _framework_.

Quindi se una libreria, per esempio, può fornire una rapida soluzione per facilitare un proprio algoritmo (i.e. fornendo una classe _ad hoc_, come per la gestione del colore), dentro un _framework_ è più facile che sia il programmatore a fornire i propri dati e i propri oggetti all'interno di una struttura formale per eseguire uno specifico compito (i. e. inserire le entità dentro una _rendering pipeline_).

Questa classificazione può aiutare a capire cosa utilizzare e come lavorare per soddisfare i requisiti di un progetto, soprattutto nella fase iniziale di sviluppo quando si calcolano le risorse a disposizione.

Una libreria è prevalentemente composta di _classi_ e _funzioni globali_. Le prime permettono di creare oggetti dalle caratteristiche non banali e ben definite. È il caso di classi per la gestione di protocolli di comunicazione, di formati multimediali, di entità matematiche e quanto altro. Le funzioni globali invece sono funzionalità autonome e vi si può accedere senza dover instanziare una specifica classe della libreria. È il caso di funzioni di conversione fra diversi formati, di invio di messaggi, di raccolta dati, o altre _routine_ di sistema che abilitano interfacce efficaci per il programmatore.

Un termine che viene spesso adottato (a volte anche fuori contesto) per interfacciarsi con delle librerie è **API** (_Application Public Interface_). In linea generale una API è un modo per raccogliere dati ed utilizzare funzioni esterne, spesso ospitate in remoto rispetto al proprio progetto (in questo caso di parla di protocolli come **REST** o **GraphQl**). In estrema sintesi si tratta di utilizzare librerie sia in maniera implicita (cioè richiedendo solo dati ad una risorsa esterna tramite un protocollo concordato) che esplicita (cioè utilizzando specifiche librerie per accedere alle API desiderate). In ogni caso, per ogni programmatore è sempre necessario avere chiaro cosa richiedere e cosa sarà ricevuto da una API, sia in termini di formato (solitamente **JSON** o **XML**) che di strutturazione della domanda e della risposta.

Utilizzare correttamente una libreria o un _framework_ è come imparare il dialetto di un linguaggio di programmazione (il caso più esemplare è la libreria **Arduino.h**), e viene da sé che quindi il modo più esaustivo adottare una nuova libreria (in programmazione si usa il termine fantascientifico _grok_) è tramite lo studio della sua documentazione (vale la pena ricordare il celebre acronimo **R.T.F.M.**). Spesso le documentazioni possono essere poco chiare, incosistenti o imprecise (soprattutto per progetti vecchi e non più mantenuti) ma vale sempre la pena cercare di appofondire le motivazioni originali e la filosofia dietro ad un progetto andando a vedere come questo è stato comunicato anche nel dettaglio tecnico.

È importante ricordare anche che moltissime librerie e _framework_ contemporanei sono progetti collettivi ed _open source_, con _repository_ pubbliche di facile e rapida consultazione, e per le quali chiunque si senta in grado di contribuire (nel migliorare le performance della libreria, nell'estendere le sue funzionalità, nel redarre la sua documentazione, etc.) lo può fare semplicemente entrando a far parte della sua comunità.

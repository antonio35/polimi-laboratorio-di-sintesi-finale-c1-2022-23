# Programmare

## Concetti

- Toolchain
- Programmazione imperativa
- Algoritmi
- Pensiero computazionale
- Diagrammi di flusso
- Pseudocodice

## Spiegazione

Ogni volta che si parla di programmazione non si intende un solo processo, ma una catena di strumenti e di lavorazioni che messe insieme permettono di trasfomare idee in codice, e quindi in programmi eseguibili.

Di fatto, un programmatore non usa mai solo un _software_ per svolgere il proprio lavoro, ma solitamente crea una "filiera" produttiva di programmi per eseguire tutti i passaggi necessari. Ad un livello minimo, per creare un programma è necessario avere:

1. Un **editor**: per scrivere il programma;
2. Un **compilatore**: per trasformarlo da file di testo in compilato ed eseguibile;

In realtà la questione dipende dal tipo di linguaggio utilizzato, dall'ambiente di sviluppo, dall'ambito di programmazione, dalla presenza di librerie esterne, dalle necessità di _debugging_, da eventuali sistemi di _CI/CD_ o di _version control_, e da molti altri fattori che possono rendere anche solo la creazione di un ambiente di lavoro molto complessa. Fortunatamente, molte di queste funzionalità vengono inglobate all'interno di **IDE** (_Integrated Developmnet Environment_).

In alcuni casi, come nella programmazione per il _web_, anche solo un _editor_ è abbastanza per svolgere il proprio lavoro, in quanto qualunque _browser_ può aprire il file sorgente in _HTML_ (e molti di questi offrono anche strumenti avanzati di debugging e monitoraggio).

Per gli scopi di questo corso, principalmente verranno creati programmi per il _web_ o per ambienti integrati, che offrono strumenti già ampiamente utilizzati e testati. Per restringere ulteriormente il nostro campo, quasi tutta la nostra programmazione sarà di tipo **imperativo**. Con programmazione imperativa si intende un paradigma di programmazione per cui il codice è visto come un serie di istruzioni intese come "ordini", e quindi eseguiti rigorosamente e consequenzialmente.

Questo modo di ragionare ci permette di vedere il codice come fosse composto delle direttive, e quindi di eseguire degli _algoritmi_, cioè delle sequenza di operazioni per risolvere una specifica classe di problemi matematici. Per fare ciò, un algoritmo deve essere _finito_, _deterministico_, _non ambiguo_, e _generale_.

Questa formulazione combacia perfettamnte con i meccanismi del _pensiero computazionale_ (o _computational thinking_), cioè l'insieme dei processi mentali utilizzati per la formulazione di un problema e della sua soluzione tramite azioni precise.

Il pensiero computazionale è composto di tre fasi:

- Formulazione del problema (_Abstraction_);
- Espressione della soluzione (_Automation_);
- Esecuzione della soluzione e valutazione della stessa (_Analysis_).

Se le prime due fasi sono solitamente opera ed ingegno dell'uomo, l'ultima può invece essere delegata alle macchine, ed in sintesi è il modo in cui vengono realizzati gli _algoritmi_ per i programmi.

Un metodo chiaro e lineare per formulare il pensiero computazione, scrivere algoritmi, e testare il loro funzionamento è tramite la creazione di _diagrammi di flusso_ (detti anche _flowchart_). Questi non sono altro che una rappresentazione grafica dei vari passaggi di un programma tramite nodi collegati da frecce direzionali.

Convenzionamente esistono 4 tipi di nodi:

- Le **ellissi** rappresentano l'inizio e la fine delle diagrammi di flusso;

![ellisse](../../Media/ellisse.png)

- I **rettangoli** rappresentano blocchi di computazione, che siano operazioni, assegnazioni, funzioni, oppure l'invio di _input_ da utente;

![rettangolo](../../Media/rettangolo.png)

- I **parallelogrammi** rappresentano i momenti in cui il programma manifesta degli _output_;

![parallelogramma](../../Media/parallelogramma.png)

- I **rombi** rappresentano le diramazioni, cioè i blocchi dalla verifica di una condizione il programma può procedere in direzioni differenti;

![rombo](../../Media/rombo.png)

Per praticità e chiarezza, i diagrammi di flusso non arrivano mai al livello di granularità delle singole righe di un codice sorgente, ma piuttosto i vari nodi racchiudono a grandi linee blocchi logici autonomi.

Di seguito è riportato un diagramma di flusso che mostra l'algoritmo per scoprire se un numero è pari o dispari:

![Pari o dispari](../../Media/pari.png)

Una diagramma di flusso è una forma di **pseudocodice**, cioè una rappresentazione più astratta del codice senza rispettare la sintasi di nessun linguaggio. Per mettere nero su bianco le idee e poterle comunicare anche a non programmatori, è utile scrivere anche pseudocodice cercando di identificare tutte le istruzioni e le funzioni da coinvolgere.

Non esiste un metodo universale di scrivere pseudocodice in quanto è un sistema più vicino al linguaggio naturale che ad una vera sintassi di programmazione, e può essere particolarmente adatto per chi deve ancora imparare bene un linguaggio, o fare pratico con il pensiero computazione e lo sviluppo di algoritmi.

In questo senso, il precedente diagramma di flusso trasformato in pseudocodice diventerebbe come segue:

```plaintext
INIZIO

Dichiarazione LOOP

Assegna a N input da tastiera

Se N % 2 è uguale a 0:  
  Stampa su schermo: "N è pari"
Altrimenti:
  Stampa su schermo: "N è dispari"

Assegna a FLAG input da tastiera

Se FLAG è positivo:
  Vai a LOOP

FINE
```

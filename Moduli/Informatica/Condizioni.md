# Condizioni

## Concetti

- If
- Else
- Operatori logici
- Operatore ternario
- Else If
- Switch

## Spiegazione

All'interno di un programma imperativo, le condizioni servono a creare delle ramificazioni che permettano di aggiungere all'algoritmo complessità, decisionalità e risoluzioni dinamiche. Tramite le condizioni, il programma è obbligato ad operare una scelta ragionata che ne modificherà il suo esito.

L'istruzione `if` serve a dichiarazione una condizione dentro un programma, e tutte le condizioni rispondono ad una domanda che può avere solo due risposte: vero o falso. Alla base di questa logica è presente l'algebra booleana, e di fatto ogni condizione risolve in un valore di tipo `bool`.

![Condizione](../../Media/condizione.png)

Un esempio di `if` è il seguente:

```c++
if ( a == b) {
  // Condizione vera
} else {
  // Condizione falsa
}
```

Subito dopo l'istruzione `if` è presente la condizione da verificare. Se la condizione è vera, allora il programma eseguira le istruzioni presenti nel blocco di codice racchiuso fra le prime parentesi graffe. Se la condizione sarà falsa, allora verrà eseguita soltanto la parente racchiusa nelle parentesi graffe dopo l'istruzione `else`.

È da notare che una condizione può omettere la parte di `else`. Se questa non c'è e la condizione fosse falsa, il programma semplicemente prosegue nell'esecuzione ignorando la condizione.

Ecco un esempio programma in **JavaScript**, il cui risultato cambia a seconda dell'ora attuale:

```javascript
let t = new Date();
let meal;
if (t.getHours() > 12) {
  meal = "pranzo";
} else {
  meal = "cena";
}
console.log("Cosa mangiamo per " + meal + "?");
```

Gli operatori che possono essere usati per costruire una condizione (anche in maniera composta) sono i seguenti:

| Operatore | Significato |
| - | - |
| ! | Negazione logica (NOT) |
| && | Congiunzione logica (AND) |
| \|\| | Disgiuzione logica (OR) |
| == | Uguaglianza |
| != | Disuguaglianza |
| > | Maggiore |
| >= | Maggiore uguale |
| < | Minore |
| <= | Minore uguale |

In alcuni linguaggi è possibile sfruttare gli operatori ternari `?:` per velocizzare l'assegnazione condizione di una variabile, come nell'esempio seguente:

```javascript
let j = a > 10 ? 20 : 5;
```

Il significato della precedente istruzione è che se `a` è maggiore di _10_ allora a `j` viene assegnato il valore _20_, altrimenti riceve il valore _5_. La prima parte, prima del `?` rappresenta la condizione, la parte fra `?` e `:` il risultato in caso vero, e la parte dopo `:` il risultato in caso falso.

Nei casi in cui una sola risposta non sia abbastanza per soddisfare le nostre casistiche, è possibile concatenare diverse condizioni sfruttando l'istruzione `else if`. Ecco un esempio:

```javascript
let t = new Date();
let meal;
if (t.getHours() < 9) {
  meal = "colazione";
} else if (t.getHours() < 12) {
  meal = "pranzo";
} else {
  meal = "cena";
}
console.log("Cosa mangiamo per " + meal + "?");
```

In questo modo, è possibile contanare successivamente diverse condizioni, e realizzare così risposte multiple per diversi casi.

![condizioni contatenate](../../Media/condizioni%20concatenate.png)

Questa soluzione, comunque, può generare già in poche istruzioni in blocchi di codice voluminois, difficili da leggere e impratici da modificare, per questo nei casi in cui la condizioni concatenate prendano a riferimento la stessa variabile può essere più conveniente utilizzare l'istruzione `switch`. Questa istruzione permette di prendere a riferimento una variabile ed eseguire diverse verifiche di uguaglianza contemporaneamente.

La sintassi di `switch` è la seguente:

```c++
switch (n) {
  case 0:
    //
  break;
  case 1:
    //
  break;
  case 2:
    //
  break;
  default:
    //
  break;
}
```

Nell'esempio di sopra, _n_ è la variabile da controllare. Ogni blocco di codice racchiuso fra `case` e `break` rappresenta una diramazione dello `switch`, da eseguire in caso di uguaglianza con il valore letterario dopo `case`. Tutti i casi che invece non vengono mappati, finiscono sotto il blocco `default`.

Tradotto in condizioni concatenate, uno `switch` avrebbe la segunte struttura.

![condizioni multiple](../../Media/conduzioni%20multiple.png)

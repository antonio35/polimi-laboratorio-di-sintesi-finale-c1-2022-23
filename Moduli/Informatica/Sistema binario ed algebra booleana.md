# Sistema binario ed algebra booleana

## Concetti

- Bit
- Sistema binario o base 2
- Operazioni binarie
- Algebra booleana
- Tabelle di verità
- Funzioni booleane

## Spiegazione

Nell'ambito dell'informatica, l'informazione è una quantità discreta che può essere misurata in **bit**. Con il termine bit ci riferiamo singole cifre in **base 2** (**BI**nary digi**T**).

Quando un numero è in base 2 - detto anche sistema binario può avere solo due valori: _0_ e _1_.

In questo modo, un numero di _n_ bit può rappresentare l'equivalente _2^n_ di valori nel sistema decimale. Ad esempio:

```text
0 = 0
1 = 1
11 = 3
111 = 7
1001 = 9
101 = 5
10 = 2
```

Il motivo per l'informatica predilige il sistema binario è perché in questo modo è possibile rappresentare, memorizzare, e svolgere operazioni con i numeri in maniera relativamente facile e scalabile tramite i circuiti elettrici.

In questo senso, il numero 0 associato al circuito aperto (nessun passaggio di corrente) mentre il numero 1 corrisponde al circuito chiuso (passaggio continuo di corrente). Altri modi per indicicare questi due stati sono: _ON/OFF_, _LOW/HIGH_, _TRUE/FALSE_.

Dentro un sistema binario è possibile eseguire operazioni - quali somma, sottrazione, etc - come in qualunque altro sistema. Ecco alcuni esempi:

```text
10 + 1 = 11
1001 + 11 = 1100
1100 - 101 = 111
```

Un modo utile per gestire i numeri in base 2 è l'**algebra booleana**. L'algebra booleana consiste di 3 operazioni logiche di base:

- **NOT**, o **!**
- **AND**, o **&&**
- **OR**, o **||**

Che servono a definire relazioni logici a qualunque livello complessità.

Per vedere come funzionano, è utile introdurre uno strumento chiamato **tabella di verità** che permette di sviluppare tutte le combinazioni di questi operatori.

L'operatore **NOT** serve ad invertire lo stato di un numero binario

| A | !A |
| - | - |
| 0 | 1 |
| 1 | 0 |

L'operatore **AND** viene utilizzato per ricavare la congiunzione logica fra 2 numeri binari. In pratica, il risultato di AND è vero solo quando entrambi i suoi operandi sono veri.

| A | B | A && B |
| - | - | - |
| 0 | 0 | 0 |
| 0 | 1 | 0 |
| 1 | 0 | 0 |
| 1 | 1 | 1 |

L'operatore **OR** viene utilizzato per riavare la disgiunzione logica di 2 numeri binari. In pratica, il risultato di OR è vero quando almeno uno dei due operandi è vero.

| A | B | A \|\| B |
| - | - | - |
| 0 | 0 | 0 |
| 0 | 1 | 1 |
| 1 | 0 | 1 |
| 1 | 1 | 1 |

La concatenazione di diversi operatori serve a costruire le **funzioni booleane**, che permettono di ottenere costruzioni logiche infinitamente complesse che possono essere replicate con circuiti elettronici, e quindi fondamentali per fini informatici.

| A | B | C | (A \|\| !B) && C |
| - | - | - | - |
| 0 | 0 | 0 | 0 |
| 0 | 0 | 1 | 1 |
| 0 | 1 | 0 | 0 |
| 0 | 1 | 1 | 0 |
| 1 | 0 | 0 | 0 |
| 1 | 0 | 1 | 1 |
| 1 | 1 | 0 | 0 |
| 1 | 1 | 1 | 1 |

# Cicli

## Concetti

- Iterazioni
- For
- While

## Spiegazione

Eseguire dei cicli è il modo in cui un programma può ripetere delle istruzioni risparmiando in performance. Oltre a ciò, dei cicli ben strutturati aumentano la leggibilità di un codice ed aiutano la comprensione del suo funzionamento.

Strutturate un ciclo consente di reiterare un blocco di codice in maniera logica e precisa, tramite l'utilizzo di una condizione di uscita. Di seguito, sotto forma di diagramma di flusso, viene rappresentato il ciclo che controlla la temperatura di una teiera fino a quando questa non raggiunge l'ebollizione.

![Ciclo](../../Media/ciclo.png)

Da notare che l'istruzione la condizione torna all'interno del flusso dopo aver aver accesso il fuoco, che altrimenti sarebbe un'operazione superflea e dispendiosa.

È fondamentale preparare e controllare adeguatamente la condizione di uscita di un ciclo, in modo da evitare l'indesiderato e dannoso fenomeno degli _infinite loop_.

La forma più semplice di dichiarazione di un ciclo è tramite l'istruzione `while`. Ecco la sua sintassi:

```c++
while (/*condizione*/) {
  //
}
```

Le partenesi dopo `while` racchudono la condizione da verificare. Funziona esattamente come nell'istruzione `if`: viene controllata lo stato logico della condizione che potrà essere `true` o `false`. Nel caso fosse vero, il programma esegue le istruzioni all'intenro del blocco racchiuso fra le parentesi graffe, e alla fine di queste viene controllata nuovamente la condizione. In caso di falsità, il ciclo viene interrotto ed il programma prosegue con le istruzioni successive.

Ecco un esempio in cui il ciclo si interrompe solo quando la funzione `random` riesce a generare un numero maggiore di numero di controllo:

```javascript
let n = 0;
let c = 0.8
let i = 0
while (n < c) {
  n = Math.random();
  i++;
}
console.log("Valore di uscita: " + n);
console.log("Numero di cicli: " + i);
```

Un metodo molto pratico ed utilizzato per eseguire un numero ben preciso di cicli è tramite l'istruzione `for`. Questa la sintassi:

```c++
for (int i = 0; i < n; i++) {
  //
}
```

Si può notare che nella istruzione `for` viene dichiarata e inizializzata una variabile detta indice (in questo caso chiamata _i_). La parte subito dopo è la condizione da verificare (in questo caso che _i_ sia inferiore ad _n_) ed infine l'indice viene incrementato di uno ad ogni iterazione. Il risultato di questa struttura è che il ciclo viene eseguito _n_ volte.

Un caso molto utile e frequente per cui viene utilizzato il ciclo `for` é la navigazione di array. Eccone un esempio, in cui array viene inizializzato dinamicamente:

```c++
int d = 10;
int n[d];
for (int i = 0; i < d; i++) {
  n[i] = d - 1;
}
```

Nulla ci vietà di utilizzare condizioni costruire un ciclo di `for` in maniera più particolare, per esempio in forma decrescente, oppure attraverso incrementi differenti, come in questo esempio

```c++
for (int i = 20; i > 0; i += 3) {
  //
}
```

Ogni linguaggio di programmazione ha inoltre i suoi metodi proprietari per eseguire dei cicli, che possono essere più adatti, leggibili, o performanti a seconda del contesto. Per esempio, con JavaScript sugli array è possibile eseguire il comando `forEach`, che naviga automaticamente dentro un array, come nel seguente esempio:

```javascript
let a = [20, 10, 15, 40];
let i = 0;
a.forEach((e) => {
  if (e < 20) { i++; }
})
console.log("Numeri inferiori a 20: " + i);
```

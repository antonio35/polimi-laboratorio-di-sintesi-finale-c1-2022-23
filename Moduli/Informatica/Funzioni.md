# Funzioni

## Concetti

- Programmazione funzionale
- Dichiarazione
- Restituzione
- Chiamata
- Parametri
- Callback

## Spiegazione

Le funzioni sono uno stratagemma molto comunque in programmazione, in quanto permettono di rendere il codice modulare, polifuzionale, e più facile da mantenere. L'idea di fondo è quella di poter racchiudere un pezzo di codice isolato, indipendente e stabile, per poterlo richiamare ovunque necessario all'interno del programma principale. In questo modo è possibile evitare variabili globali (che sono spesso causa di problemi), ripetizione del codice, e soluzioni poco pratiche per performance e leggibilità.

Ogni linguaggio di programmazione gestisce la dichiarazione di funzioni in maniera differente. Dentro **JavaScript** la cosa è particolarmente semplice, basta usare l'istruzione `function`, scegliere un nome univoco seguito da parentesi tonde, e dichiarare il codice della funzione dentro parentesi graffe.

```javascript
function myFunction() {
  //
}
```

Nella sua dichiarazione, può essere presente qualunque tipo di codice, ma a meno di non utilizzare solo variabili globali (cosa non raccomandata nella programmazione funzionale) o richiedere comportamenti isolati, allora è utile fare si che la funzione restituisca un valore. Questo viene fatto tramite l'istruzione `return`, che deve essere l'ultima prima della chiusura della fuzione. Ecco un esempio di funzione che restituisce una stringa con l'ora attuale:

```javascript
function whatTimeIsIt() {
  let t = new Date();
  let s = Date.getHours() + ':' + Date.getMinutes(); 
  console.log('Time is ' + s);
  return s;
}
```

In caso delle istruzioni fossero scritte dopo il `return`, queste verrebbero ignorate. È possibile anche avere più `return` all'interno della stessa funzione (utile nel caso di diramazioni con `if` e `switch`).

Se la funzione è correttamente dichiarata, allora è possibile chiamarla all'interno del nostro codice sorgente semplicemente riportanto il suo nome.

```javascript
myFunction();
let x = myFunction();
```

Molto comunemente, alle funzioni vengono passati di parametri, da riportare dentro le parentesi tonde. Questi parametri (detti _function signature_) vanno esplicitati nella dichiarazione della funzione, e richiamati durante la sua esecuzione. In questo modo è possibile utilizzare lo stesso pezzo codice con differenti condizioni iniziali, ed ottenere quindi diversi risultati. Nell'esempio seguente, la funzione `isEven()` accetta un parametro di nome `a`, e restituisce una variabile booleana. Se `a` è pari il risultato di `isEven()` è `true`, se è dispari è `false`.

```javascript
function isEven(a) {
  return !(a%2);  
}
```

È possibile anche dichiarare funzioni con lo stesso nome ma con differenti _signature_ (in questo caso si parla di _signature override_), in modo da avere diversi comportamenti a seconda dei parametri forniti.

In alcuni linguaggi, come in javascript, è possibile usare funzioni utilizzate come parametri di altre funzioni, ed in certi casi sono persino obbligatorie per il loro corretto svolgimento. È il caso delle funzioni di _callback_, che servono a dichiarare quale comportamento deve seguire una certa funzione quando viene chiamata.

Per esempio, nel codice di seguito, viene dichiarata una funzione come parametro della funzione `reduce()`, che si applica iterativamente su tutti gli elementi di un _array_ (in questo caso `tmp`).

```javascript
function removeChar(string, char) {
  let tmp = string.split(char);
  let tmp = tmp.reduce(function(previousValue, currentValue){
    return previousValue.concat(currentValue);
  }, '');
  return tmp;
}
```

Maggiori dettagli su quali funzioni richiedono _callback_ e come operano con esse sono forniti nella documentazione del linguaggio.

# Oggetti

## Concetti

- Programmazione orientata agli oggetti
- Classi
- Parametri
- Metodi
- Costruttore
- JSON

## Spiegazione

La _programmazione orientata ad oggetti_ (detta anche _OOP_) è un paradigma in cui lo sviluppo degli algoritmi si basa sugli, cioè entità autonome con metodi e proprietà che permettono di simulare modelli reali e conservare informazioni in maniera sistematica. I vantaggi della _OOP_ sono racchiusi in tre concetti:

- Eredità
- Polimorfismo
- Incapsulamento

Questi permettono di sviluppare classi e sistemi relazonali in maniera altamente flessibile ed infinitamente scalabile.

In programmazione, una classe è la definizione generica di quello che sarà instanziato come un oggetto. La classe cioè è l'elemento astratto, mentre l'oggetto è l'elemento concreto. Per esempio, per realizzare un'entità che descriva un qualunque tipo di auto, allora è necessario stilare tutte le caratteristiche comuni a tutte quei tippi di veicoli, come marca, modello, dimensioni, peso, etc.

Queste caratteristiche si chiamano _parametri_, e servono a definire gli attributi di una classe Sono variabili che vengono dichiarate nella definizione della classe e possono essere richiamate e manipolate nel corso del programma.

Di seguito, un esempio di classe per auto scritto in **C++**:

```c++
class car {
  public:
    int weight;
    char[] owner;
  private:
    unsigned int year;
    char[] brand;
    char[] model;
    int[3] dimensions;
    bool insurance;
};
```

Nel proprio codice sorgente, per creare un oggetto da questa classe, sarà necessario dichiarare una variabile utilizzando come tipo il nome della classe.

```c++
car myCar;
```

Si può notare nell'esempio precedente che alcune variabili sono dichiarate sotto la l'istruzione `public`, altre sotto `private`. La differenza sta che le prime sono variabili sono a cui è possibile accedere anche pubblicamente all'interno del codice, richiamandole come fossero degli attributi dell'oggetto, mentre le seconde - essendo privati - non sono pubblicamente accessibili, ma possono essere richiamate e modificate solo tramite metodi pubblici della classe stessa.

Di fatto, una classe può possedere anche dei _metodi_, cioè delle funzioni dedicate che servono ad aggiornare i propri parametri oppure a fornire un'utilità al codice principale.

Ogni classe implicitamente possiede già un metodo chiamato _costruttore_ che serve ad inizializzare i parametri della classe nel momento della sua instanziazione in oggetto. Il costruttore può essere dichiarato esplicitamente, prevedendo così dei comportamenti personalizzati nella dichiarazione di un oggetto. In caso non lo fosse, i parametri dell'oggetto vengono inizializzati come da loro comportamento originale.

Di seguito, viene mostrato come vengono dichiarati alcuni metodi nella stessa classe per auto.

```c++
class car {
  public:
    car();
    void setDimensions(int[3] d);
    int weight;
    string owner;
    bool checkInsurance();
    void setInsurance(bool state);
  private:
    unsigned int year;
    string brand;
    string model;
    int[3] dimensions;
    bool insurance;
};
```

Il funzionamento dei metodi, deve essere dichiarato separatamente, proprio come se si definissero delle funzioni. In C++, in questa fase dichiarativa, è fondamentale aggiungere al nome del metodo anche il nome della classe (più tecnicamente, lo _scope_ del metodo) così da esplicitare la sua appertenza. Di seguito, vengono definiti i metodi della classe precedentemente dichiarata:

```c++
car::car() {
  weight = 0;
  year = 0;
  dimensions[0] = 0;
  dimensions[1] = 0;
  dimensions[2] = 0;
  insurance = false;
}

void setDimensions(int[3] d) {
  dimensions[0] = d[0];
  dimensions[1] = d[1];
  dimensions[2] = d[2];
}

bool car::checkInsurance() {
  return insurance;
}

void car::setInsurance(bool state) {
  insurance = state
}
```

A questo punto, nel mio codice, se ho un oggetto istanziato correttamente posso richiamare anche i suoi parametri ed i suoi metodi pubblici:

```c++
car myCar;
int[3] tmp = { 130, 200, 180 };
myCar.setDimensions(tmp);
myCar.owner = "me";
bool insurance = myCar.checkInsurance();
```

Nel linguaggio **JavaScript** esiste un sistema chiamato **JSON** (_JavaScript Object Notification_) che permette di creare rapidamente oggetti composti da coppie di chiavi ed attributi, facili da leggere, manipolare e comunicare. Questa visione è ben differente dal modo di intendere di classi ed oggetti come descritto fin ora (tecnicamnte, i JSON sono più vicini agli _array_), ma per via della loro semplicità di uso i JSON sono utilizzati ubiquitamente nel mondo dell'informatica.

Dentro un JSON una _chiave_ è una stringa univoca che serve ad identificare un parametro. Ogni chiave possiede un _attributo_ che è il contenuto di una variabile di tipo `int`, `float`, `bool`, `string`, `array`, `object`. Ecco un esempio di JSON per descrivere un veicolo:

```json
{
  brand: "Fiat",
  model: "500",
  year: 1998,
  features: ["4WD", "AC", "Hydraulic suspensions"],
  owner: {
    name: "Susan",
    surname: "Hermann",
    purchaseYear: 2003
  },
  insurance: true
}
```

Spesso i JSON vengono salvati come stringhe dentro file ed utilizzati come rudimentale sistema di archiviazione. JavaScript, in questo senso offre molte funzioni per condificare e decodificare stringhe in oggetti.

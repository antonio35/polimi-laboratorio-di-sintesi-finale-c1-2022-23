# Variabili

## Concetti

- Variabile
- Tipi primitivi
- Dichiarazione
- Assegnazione
- Array

## Spiegazione

In informatica, una variabile è uno spazio di memoria che viene assegnato durante l'esecuzione di un programma per conservare un'informazione. Questa informazione può essere consultata, manipolata, sovrascritta o cancellata.

Tutte la variabili, ad un livello fisico, sono immagazzinate come una combinazione di **byte** (un byte corrisponde ad 8 bit), ma il modo in cui vengono interpretati dal programma può dare un significato radicalmente differente ad ogni variabile.

Ogni variabile possiede 3 componenti:

1. Un identificativo
1. Una tipologia
1. Un valore

L'**identificativo** è il suo nome, l'etichetta univoca che serve a richiamare quella variabile all'interno di un programma.

La **tipologia** invece è il tipo di informazione che una variabile rappresenta. Ogni linguaggio di programmazione possiede i suoi tipi di variabili, ma le tipologie dette _primitive_ e (quasi) universali per tutti i linguaggi sono:

- **Bool**: detto anche `boolean`, rappresenta i numeri in base 2.
- **Integer**: detto anche `int`, rappresenta un numero intero. Può avere anche valore negativo (a meno che non venga accompagnato dal prefisso `unsigned`, nel qual caso sarebbe solo positivo) e la sua dimensione minima o massima dipende dal compilatore del programma (solitamente ha un'estensione di _2 byte_)
- **Float**: rappresenta i numeri decimali (_floating point number_). Anche in questo caso la dimensione del numero dipende anche dal compilatore, che ne definisce anche la risoluzione (cioè il numero di posizioni decimali che è possibile utilizzare)
- **Character**: detto anche `char`, rappresenta un singolo carattere codificat in [**ASCII**](https://www.asciitable.it/). Non si tratta solo di numeri e lettere, ma anche di simboli e caratteri di controllo. Catene di `char` vengono solitamente riferite come **String**.

Il **valore** è la specifico numero salvato in memoria per una variabile, il suo contenuto. Come detto, a seconda della tipologia di variabile, il valore può rappresentare valori molto differenti. Ad esempio, una variabile di tipo `char` uguale a _1_ se trattata come tipo `int` ha valore _49_, oppure una variabile tipo `float` con valore _2.34_ se trattata come `bool` ha valore _false_ (anche se quest'ultimo caso dipende dal modo in cui il compilatore gestisce il codice sorgente).

Per creare una variabile è necessario dichiararla. Durante la dichiarazione vengono esplicitati il suo identificativo ed il suo valore (il valore può anche essere omesso in fase di dichiarazione, il quel caso si dice che la variabile è _non definita_ o `undefined`). Nei linguaggi di programmazione detti a _strong typing_ i tipi di variabile devono essere sempre dichiarati. Per esempio, in un programma in **C++** le dichiarazioni sono scritte nella seguente maniera:

```c++
int a = 10;
bool b = false;
float c = 0.3
char f = 'a';
```

Nei linguaggi di programmazione che supportano il _weak typing_ invece il tipo di variabile viene ricavato dal contesto, e la tipologia stessa può cambiare a seconda delle operazioni coinvolte. È il caso di **JavaScript**, dove è possibile dichiarare le variabili nel seguente modo:

```javascript
let a = -30;
let b = true;
let c = 4.23;
let d = 'z'
```

Il simbolo di uguale, `=`, viene utilizzato - sia durante le dichiarazoni che nelle successive istruzioni - per assegnare un valore ad una variabile. Questo può avvenire tramite l'utilizzo di valori _letterari_ (cioè espliciti), oppure tramite operazione e l'utilizzo di altre variabili. Ecco alcuni esempi di assegnazione.

```c++
int a = 10;
int b;
b = 20 + 10;
a = b * 2;
```

La maggior parte dei linguaggi di programmazione rispetta una sintassi derivata e simile al linguaggio **C**, e rispetta gli stessi principi legati a dichiarazioni, assegnazioni ed operazioni. Ma variazioni sintattiche più o meno sottili possono far differire in maniera sostanziale il funzionamento di diversi linguaggi, quindi è sempre fondamentale essere sicuri di come utilizzare gli strumenti a propria disposizione.

Di frequente, in programmazione si presenta la necessità di raggruppare e gestire insiemi di variabili dello stesso tipo contemporaneamente (ad esempio: un gruppo di nomi, le precipitazioni di una zona in un periodo temporale, l'insieme delle configurazioni degli interruttori di un edificio, etc.). In questi casi, lo strumento più adatto sono gli **array**, che permettono di richiamare un numero dinamico di elementi sotto la stessa variabile, i quali condividono tipologia, identificativo, ma non vaore.

Ad esempio, se devo registrare le temperatura media di un termometro per ogni stagione, potrei sfruttare un array nel seguente modo:

```c++
int temp[4];
temp[0] = 10;
temp[1] = 18;
temp[2] = 28;
temp[3] = 15;
```

Le parentesi quadre ed il numero _4_ nella dichiarazione di `temp` stanno a significare che questa variabile è un array di tipo `int`, con quattro posizioni dichiarate. Le successive 4 istruzioni servono ad assegnare tutte le posizioni dell'array, in questo caso le parentesi quadre servono ad accedere ad una precisa posizione dell'array. È importante notare che negli array si comincia a contare le posizioni da _0_ (e quindi l'ultima posizione di un array di dimensione _n_, è _n-1_).

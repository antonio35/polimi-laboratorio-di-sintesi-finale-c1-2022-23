# Modulo

## Concetti

- Comunicazione seriale
- SerialControl
- Inviare
- Ricevere

## Spiegazione

**p5.js** nasce come _porting_ di **Processing** per lo sviluppo web, e per questo integra anche la possibilità di gestire messaggi tramite dispositivi collegati nelle porte **USB**, e quindi tramite il protocollo della comunicazione seriale, grazie ad una classe di funzioni dedicate.

Per permettere queste funzionalità è necessario avere un programma che faccia da _websocket_. cioè che consenta di interfacciare in tempo reale una porta seriale con il programma in esecuzione su _browser_.

Gli sviluppatori dietro a p5.js hanno già implementato un programma chiamato **SerialControl** per questo scopo, facilitando la vita a altri programmatori, ed è possibile scaricarlo dalla [relativa repository](https://github.com/p5-serial/p5.serialcontrol/releases/).

![SerialControl](../../Media/p5__communication__serialcontrol.png)

Una volta scaricato ed eseguito il programma, e collegato il dispositivo scelto nella porta seriale, è necessario scegliere la porta dedicata a questa operazione nel relativo elenco, quindi viene generato un pezzo di codice da copiare nel proprio _sketch_ di p5.js per cominciare a programmare sfruttando la porta seriale.

![SerialControl](../../Media/p5__communication__starter.png)

Questo _template_ di programma offre diverse funzioni per adattare il proprio algoritmo alla comunicazione seriale, che sono collegate alla classe per la porta seriale tramite le funzioni `serial.on()`.

Se si desidera operare più a basso livello, dentro lo _sketch_ è possibile inviare messaggi su porta seriale tramite l'istruzione `Serial.print()`, nel seguente modo:

```javascript
serial.write('This is a message');
```

Lavorando con **JavaScript** è possibile sfruttare le funzionalità avanzate di questo linguaggio nella manipolazione delle stringhe, e quindi facilmente concatenare anche tipi di variabili differenti fra loro nello stesso messaggio.

Se la comunicazione invece è in ricezione, è possibile utilizzare la funzione `Serial.read()`, che permette di ricevere un _byte_ alla volta. Anche nel caso di ricezione di caratteri che però codificano numeri, è possibile sfruttare le flessibili capacità di conversione fra tipi di JavaScript per fare _parsing_ dei messaggi ricevuti.

```JavaScript
let inData = serial.readLine();
```

# Hello, World

## Concetti

- Componenti di un progetto in p5.js
- Struttura di uno sketch
- Console

## Spiegazione

**p5.js** funziona dentro pagine **HTML**. Nello specifico, vive come _script_ all'interno di pagine HTML, quindi è possibile creare un singolo file con questa estensione per avere far partire un progetto in p5.js. Se questa soluzione è comoda e veloce per esperimenti e semplici _rendering_, con progetti di medio-grandi dimensioni è molto più sensato strutturare il lavoro più modularmente.

Di fatto, un tipico progetto di p5.js dovrebbe vivere in una cartella con una struttura simile alla seguente:

```yaml
- Folder:         # cartella di progetto
  - index.html    # Pagina iniziale
  - sketch.js     # Script in p5.js
  - style.css     # foglio di stile
  - p5:           # cartella libreria
    - p5.js       # codice sorgente di p5.js
```

Ed il file `index.html` dovrebbe avere almeno il seguente contenuto:

```html
<html>
  <head>
    <script src="p5/p5.js"></script>   
    <script src="sketch.js"></script>   
    <link rel="stylesheet" href="sytle.css">
  </head>
  <body>
    <main>
    </main>
  </body>
</html>
```

Questa non è una struttura normativa, ma solo un'indicazione di come gestire al meglio un progetto secondo un metodo condiviso con anche altri tipi di lavori.

Il file `sketch.js` è dove effettivamente vivrà il codice del progetto (il nome _sketch_ è comune in tanti progetti e deriva dal termine utilizzato in **Processing** per identificare una'applicazione. Il file può comunque chiamarsi in qualunque modo).

Uno sketch di p5.js è composto da due funzioni chiamate `setup` e `draw`. Dentro queste vive tutto il programma per realizzare i nostri contenuti multimediali. `setup` viene eseguito una volta sola all'inizio dell'esecuzione, è il modo in cui viene impostato inizialmente il sistema. `draw` invece viene chiamato e ripetuto un numero infinito di volte dopo l'esecuzione di `setup`. Dentro `draw` vive il programma effettivo, dove vengono interpretati gli input, viene aggiornato lo stato del sistema, e vengono renderizzati gli output. Riavviando la pagina del progetto, lo sketch comincerebbe di nuovo da `setup` e poi continuerebbe a ciclare dentro `draw`.

Per il nostro programma **Hello, World** vogliamo sfruttare entrambi. Ecco il codice completo di `sketch.js`:

```javascript
let canvas = {
  x: 200,
  y: 200
}
let fontSize = 32;
let message, coord;

function setup() {
  createCanvas(canvas.x, canvas.y);
  message = "Hello, World!";
  coord = {
    x: 10,
    y: 30
  };
  textSize(fontSize);
}

function draw() {
  text(
    message,
    coord.x,
    coord.y,
  );
}
```

Innanzitutto, vengono dichiarate delle variabili a livello globale: `canvas` per la dimensione del nostro spazion di lavoro, `fontSize` per la dimensione del testo su schermo, e `message` e `cord` per gestire i parametri del testo.

Dopodichè viene definita la parte di `setup`. Dentro questo blocco di codice viene creato l'ambiente di lavoro con `createCanvas` (passa i parametri di `canvas` per dimesionare lo spazio), quindi vengono assegnati dei valori a `message` e `coord`, infine viene dichiarato quanto dovrà essere grande il testo da mostrare con `textSize`.

Dentro il blocco di `draw` invece è presente solo un'istruzione, la funzione `text`. Questa accetta 3 parametri: il primo per il contenuto del testo, gli altri 2 per le coordinate su cui posizionarlo dentro il nostro _canvas_.

In questo modo, `draw` stampa ripetutamente su schermo il messaggio che abbiamo impostato assegnato alla variabile `message`.

![hello world](../../Media/hello%20world.png)

Un'altro modo per mostrare dei messaggi è quello di sfruttare l'API per `console` di **JavaScript**. Questo metodo permette fra le altre cose di stampare messaggi nella sezione del _browser_ dedicata alle informazioni di sistema e di debug, accessibile tramite gli strumenti per ispezionare una pagina web. Basta aggiungere una istruzione come la seguente dentro il proprio file di JavaScript per stampare su _console_

```javascript
console.log('Hello, World!');
```

In questo modo, è possibile inviare e controllare messaggi - anche dinamici - senza dover impattare nella resa multimediale del progetto.

![console](../../Media/console.png)

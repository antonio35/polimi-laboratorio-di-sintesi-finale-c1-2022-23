# Forme

## Concetti

- Coordinate spaziali
- Punto
- Linea
- Rettangolo
- Ellisse

## Spiegazione

Prima di poter disegnare qualcosa su un _canvas_ di **p5.js** è importante sapere che l'origine delle coordinate cartesiane utilizzate per i _rendering_ dello _sketch_ è nel punto in alto a sinistra del _canvas_, con valore _(0, 0)_. In caso il programma dovesse far partire gli assi da altri punti, sarà necessario creare una funzione di conversione per gestire la traslazione delle coordinate.

p5.js offre diverse funzioni per rappresentare velocemente le principali forme di geometria planare. Queste sono:

- `point(x, y)`: per disegnare un punto presso le coordinate _(x, y)_;
- `line(x1, y1, x2, y2)`: per disegnare una linea che parte da _(x1, y1)_ fino a _(x2, y2)_;
- `rect(x, y, w, h)`: per disegnare un rettangolo con origine su _(x, y)_, larghezza _w_ ed altezza _h_. Il modo in cui viene gestita l'origine è tramite la funzione `rectMode()`;
- `ellipse(x, y, w, h)`: per disegnare un'ellisse con origine su _(x, y)_, larghezza _w_ ed altezza _h_. Il modo in cui viene gestita l'origine è tramite la funzione `ellipseMode()`;

Ecco uno sketch in cui sono presenti tutte queste forme, disegnate dentro un cavans di lato _500px_:

```javascript
function setup() {
  createCanvas(500, 500)
  strokeWeight(5);
  point(100, 100);
  line(350, 50, 450, 150);
  rectMode(CENTER);
  rect(400, 400, 50, 70);
  ellipse(100, 400, 30, 50);
}
```

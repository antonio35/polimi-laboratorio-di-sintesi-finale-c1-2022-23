# Modulo

## Concetti

- Gestione colore
- Sfondo
- Riempimento
- Contorno

## Spiegazione

All'interno di **p5.js** il colore può essere gestito come **RGB** o come **HSL**. Questa opzione viene definita tramite la funzione `colorMode()` che richiede due parametri:

- `mode`: `RGB` o `HSL` (anche `HSB`);
- `max`: il margine massimo per il valori a partire da _0_ (di default è _255_);

Successivamente alla chiamate di `colorMode()`, tutte le funzioni riguardanti l'utilizzo dei colori accetteranno i parametri concordati.

Per esempio, è possibile assegnare un colore di sfondo al _canvas_ principiale, tramite la funzione `background()`. Con modalità di gestione del colore di default (RGB con limite a _255_), uno sfondo completamente bianco viene gererato con l'istruzione `background(255, 255, 255)`.

Oltre al _canvas_, ogni forma o carattere di p5.js possiede due parametri estetici il cui colore può essere gestito dinamicamente: il riempimento ed il contorno. Questi vengono assegnati rispettivamente tramite le funzioni `fill()` e `stroke()`.

Dal momento in cui queste vengono chiamate, tutte le forme generate successivamente prenderanno queste caratteristiche estetiche. Del contorno inoltre può essere gestito lo spessore in _pixel_ tramite la funzione `strokeWeight()`.

Nell'esempio successivo ai 4 quadrati vengono assegnati 4 colori di contorno e riempimento differenti:

```javascript
function setup() {
  createCanvas(200, 200);
  strokeWeight(10);
  rectMode(CENTER);
  
  rect(100, 100, 95, 95);
  
  fill(8, 126, 139);
  stroke(244, 250, 255);
  rect(300, 100, 95, 95);
  
  fill(200, 29, 37);
  stroke(255, 90, 95);
  rect(100, 300, 95, 95);
  
  fill(11, 57, 84);
  stroke(191, 215, 234);
  rect(300, 300, 95, 95);
}
```

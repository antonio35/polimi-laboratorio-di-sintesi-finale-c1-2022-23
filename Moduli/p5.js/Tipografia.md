# Tipografia

## Concetti

- Stringhe
- Testo
- Parametri

## Spiegazione

Per utilizzare del testo all'interno di uno _sketch_ è necessario avere a che fare con le **stringhe**, cioè i tipi di variabili per salvare e gestire insiemi caratteri. A differenza di altre variabili, la manipolazione di stringhe avviene tramite funzioni _ad hoc_, mentre altri operatori si comportano in maniera variabile a seconda delle operazioni coinvolte. È importante tenere a mente che **JavaScript** offre diverse funzioni per interfacciarsi con le stringhe - come `toString()` o `toNumber()`, che conviene sempre utilizzare piuttosto che rischiare comportamenti imprevedibili.

La funzione di **p5.js** utilizzata per stampare del testo su schermo è `text()`, che accetta i seguenti parametri:

- `str`: la stringa da stampare;
- `x`: coordinata x del testo;
- `y`: coordinata y del testo;
- `y2`: altezza del _box_ di testo;
- `x2`: larghezza del _box_ di testo;

Ecco un esempio di text() per stampare l'ora attuale:

```javascript
function setup() {
  createCanvas(400, 400);
  let time = new Date();
  let t = time.getHours() + ':' + time.getMinutes();
  text(t, 200, 200);
}
```

Proprio come le altre forme in p5.js, anche gli elementi di testo prendono gli attributi definiti dalle funzoni `fill()`, `stroke()`, e `strokeWeight()`. Oltre a questi è possibile gestire ulteriori parametri tipografici tramite diverse funzioni proprietarie. Alcune di queste sono:

- `textSize()`: per impostare la dimensione del carattere in pixel;
- `textStyle()`: per impostare uno stile al carattere;
- `textAlign()`: per impostare l'allineamento del carattere, sia orizzontalmente che verticalmente;

Eccole all'opera:

```javascript
function setup() {
  createCanvas(500, 500);
  textSize(60);
  noStroke();  

  textStyle(ITALIC);
  textAlign(LEFT, CENTER);
  fill(50, 50, 50);
  text('Text', 200, 70);
  
  textStyle(NORMAL);
  textAlign(CENTER, CENTER);
  fill(180, 180, 180);  
  text('Text', 200, 250);

  textStyle(BOLD);
  textAlign(RIGHT, CENTER);
  fill(255, 255, 255);
  stroke(0, 0, 0);  
  strokeWeight(5);
  text('Text', 200, 430);
}
```

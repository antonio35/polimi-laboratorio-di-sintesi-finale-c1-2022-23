# Canvas

## Concetti

- Canvas HTML
- Creazione
- Ridimensionamento
- Salvataggio

## Spiegazione

L'elemento principale sui cui lavora uno _sketch_ di **p5.js** è il _canvas_, cioè un componente di **HTML** che permette la creazione dinamica di contenuti grafici tramite codice. È possibile disegnare su di un _canvas_ grazie ad una serie di funzioni a basso livello in **JavaScript**, ma sono generalmente difficili da leggere ed utilizzare in maniera rapida. Di fatto, p5.js non fa altro che costruire un livello di astrazione sopra queste funzioni, più facile da gestire e manipolare, che permette di creare rese grafiche con meno righe di codice e di più facile interpretazione (questo modo di scrivere librerie viene spesso riferito come _zucchero sintattico_).

Dentro il codice dello sketch, tramite la funzione `createCanvas()` viene creato durante il _runtime_ un elemento `<canvas>` dentro l'elemento `<main>` (se quest'ultimo non è presente, viene generato sul momento dentro `<body>`).

`createCanvas()` prevede anche alcuni parametri:

- **w**: per la larghezza del canvas
- **h**: per l'altezza del canvas

Nell'esempio succesivo, viene creato un _canvas_ di lato 600*500 pixel:

```javascript
function setup() {
  createCanvas(600, 500);
}
```

Per sfruttare la natura _responsive_ del web, è possibile utilizzare la funzione `resizeCanvas()` per aggiustare la dimensione di _canvas_ durante l'esecuzione del programma. Per esempio, qui sotto `resizeCanvas()` viene utilizzato in coppia con `windowResized()` - una funzione che viene chiamata ad ogni cambio di dimensione del _browser_ - per ridimensionare dinamicamente _canvas_ alla finestra dell'applicazione:

```javascript
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
```

Una funzione molto utile per scopi di documentazione è `saveCanvas()`, in quando permette di scattare un'istantanea di _canvas_ e di salvarlo come file _raster_.

```javascript
saveCanvas('screenshot', 'png');
```

Nell'esempio precedente, _canvas_ viene esportato come un file dal nome **screeshot.png**.

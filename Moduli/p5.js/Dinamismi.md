# Dinamismi

## Concetti

- Progressioni
- Limiti
- Trigonometria
- Tempo
- Animazioni

## Spiegazione

Sfruttando le istruzioni di ogni linguaggio di programmazione, e le funzioni e la struttura di uno _sketch_ in **p5.js**, è possibile creare rapidamente dei blocchi di codice generativo che permettano la resa di configurazioni grafiche complesse agendo solo su posizione _posizione_, _dimensioni_ e _colori_.

Il sistema più semplice è quello di sfruttare i cicli per generare insiemi di entità differenti ma correlate fra di loro, come nel seguente esempio in cui programmaticamente vengono creati cerchi con posizioni differenti:

```javascript
let offset = 50;
let distance = 40;
let radius = 20;
let n = 10;

function setup() {
  createCanvas(500, 500);

  for (let i = 0; i < n; i++) {
    ellipse(
      (i * distance) + offset,
      offset,
      radius,
      radius
    );
  }
}
```

La stessa logica può essere applicata anche a più dimensioni con più cicli, e sfruttando anche delle condizioni per avere effetti sofisticati, come nel seguente esempio dove viene generata una griglia di cerchi ed i cerchi di coordinata uguale prendono un colore differente.

```javascript
let offset = 50;
let distance = 40
let radius = 20;
let n = 10;
let m = 10;

function setup() {
  createCanvas(500, 500);

  for (let i = 0; i < n; i++) {
    for (let k = 0; k < m; k++) {
      if (i == k) {
        strokeWeight(10);
        stroke(100, 20, 240);
        fill(43, 200, 180);
      } else {
        strokeWeight(2);
        stroke(255, 255, 255);
        fill(1, 1, 1);
      }
      ellipse(
        (k * distance) + offset,
        (i * distance) + offset,
        radius,
        radius
      );
    }
  }
}
```

In tutti i casi di creazione generativa, per evitare cicli infiniti e grandi confusioni, è sempre utile avere a mente quali sono i limiti della nostre parti generative. Negli esempi di sopra i limiti sono dati dal dominio dei cicli, oppure è possibile prevedere condizioni concatenate fra di loro per creare sistemi ben più complessi, l'importante è non lasciare che un programma possa esaurire risorse o bloccarsi perchè non sono stati previsti - o sono stati previsti male - i suoi limiti di azione.

Entrambi i casi visti in precedenza sono di _crescita lineare_, in cui i parametri crescono proporzionalmente con le iterazioni del codice, ma con un po' di matematica applicata ed un po' di prove è molto facile sviluppare crescite non-lineari, logaritmiche, esponenziali, e così via, per creare effetti non banali e di grande resa.

Uno strumento molto utile di lavoro è la _trigonometria_, in quanto consente di creare cicli periodici di parametri da sfruttare per armonizzare i risultati grafici. Un esempio molto concreto per esempio è la generazione di sequenze di colori. **JavaScript** fornisce velocemente molte funzioni trigonometriche tramite la libreria `Math`, che possiamo vedere in azione nel seguente _sketch_ in cui viene generata una sequenza armonoisa di colori sfruttando la funziona `sin()`:

```javascript
let centerOffset = 100;
let radius = 20;
let n = 50;
let rOffset = 0;
let gOffset = 90;
let bOffset = 180;
let colorOffset = 200;

function setup() {
  createCanvas(500, 500);

  for (let i = 0; i < n; i++) {
    let posX = Math.sin(((Math.PI * 2) / n) * i) * centerOffset;
    let posY = Math.cos(((Math.PI * 2) / n) * i) * centerOffset;

    fill(
      (Math.sin(i + rOffset) * 255) + colorOffset,
      (Math.sin(i + gOffset) * 255) + colorOffset,
      (Math.sin(i + bOffset) * 255) + colorOffset
    );

    ellipse(
      posX + 250,
      posY + 250,
      radius,
      radius
    );
  }
}
```

Un altro meccanismo molto utile per generare dinamismi efficaci è quello di sfruttare il tempo del programma, non solo per generare entità a scarti temporali predefiniti, ma anche per creare animazioni. Per questo scopo, viene molto utile utilizzare la funzione `millis()`, che quando chiamata restituisce il tempo trascorso in millisecondi dall'avvio di `setup()`. Nel seguente esempio, `millis()` viene stampato su _console_ per tenere traccia dello scostamento temporale fra due successive funzioni `draw()`:

```javascript
let t = 0
function setup() {
  t = millis();
}
function draw() {
  console.log(millis() -  t);
  t = millis();
}
```

Come si può notare da questo esempio, il risultato di questo tempo però può differire molto da un _frame_ con l'altro e non ci permette di creare dinamismi gradevoli (oltre a poter creare problemi di _performance_). Per rendere `millis()` utile è necessario sfruttare il concetto di **Delta**, cioè confrontare la quantità di tempo trascorsa ogni due frame di uno _sketch_ con un valore predefinito. Nel caso in cui _Delta_ risulti maggiore della durata predefinita, allora può essere effettuato il _rendering_. Solitamente questo viene fatto tramite il seguente meccanismo:

```javascript
let t = 0
let frame = 40; // 1/25th of second

function setup() {
  t = millis();
}

function draw() {
  if (millis() - t > frame) {
    t = millis();
  }
}
```

Tramite queesto meccanismo, in collaborazione con la funzione `clear()` che permtte di ripulire un _canvas_, è possibile strutturare diversi tipi di animazioni. Nel seguente _sketch_, il cerchio ingrandisce e si riduce seguento questa logica:

```javascript
let t = 0
let frame = 40; // 1/25th of second
let r = 0;
let canvasSide = 500;
let direction = true;

function setup() {
  createCanvas(canvasSide, canvasSide);
  strokeWeight(3);
  t = millis();
}

function draw() {
  if (millis() - t > frame) {
    if (direction) {
      if (r < canvasSide) { r++; }
      else { direction = false; }
    } else {
      if (r > 0) { r--; } 
      else { direction = true; }
    }
    clear();
    ellipse(canvasSide/2, canvasSide/2, r, r);
    t = millis();
  }
}
```

# Setup

## Concetti

- p5.js vs Processing
- Package
- CDN
- Template
- Ambiente di test

## Spiegazione

**p5.js** è una libreria di **JavaScript** inspirata all'ambiente di sviluppo **Processing**, utilizzato per scrivere programmi con scopi multimediali.

Il vantaggio di avere una libreria di un linguaggio piuttosto che un sistema _ad hoc_ sta nell'estrema portabilità offerta dai programmi scritti in Javascript, questo perché essere testati e fatti funzionare serve soltanto un _browser_ (al contrario, un programma in Processing deve essere compilato specificatamente per il sistema operativo di destinazione, facendo attenzione a versioni, requisiti e molte altre complessità).

Oltre a ciò, poter sfruttare la grande versatilità della programmazione per il web insieme alle potenzialità di p5.js apre le porte ad infinite possibilità di sviluppo.

L'altro lato della medaglia è la necessità di dover gestire più linguaggi di programmazione in contemporanea (quanto meno **HTML** e **CSS** per il front-end), con la conseguente curva di apprendimento più ripida. In ogni caso, tutte i concetti e gli strumenti imparati con Processing possono essere adattati con minimo sforzo a p5.js.

Esistono due possibilità per includere p5.js dentro un progetto: tramite _package_ oppure tramite **CDN**.

Il primo metodo consiste nello scaricare la libreria [dal sito ufficiale](https://p5js.org/download/) oppure tramite [gestore di pacchetti](https://www.npmjs.com/package/p5), e fare in modo che la pagina HTML che ospiterà il progetto abbia i relativi collegamenti ai file della libreria. Di seguito è fornito un esempio di questo approccio:

```html
<html>
  <head>
    <script src="p5/p5.min.js"></script>  
  </head>
  <body>
  </body>
  
</html>
```

L'istruzione `<script src="p5/p5.min.js"></script>` è la riga che punta al file con la libreria _minificata_ di p5.js (in questo caso, il file `p5.min.js` è dentro una cartella nella stessa posizione del file HTML). Il vantaggio di questo approccio è quello di avere tutto il codice sorgente presente nella cartella del progetto, e quindi idealmente questo può funzionare anche offline.

L'altro metodo è tramite **CDN** (**Content Delivery Network**), cioè richiamando la libreria da uno [URL statico sul web](https://cdn.jsdelivr.net/npm/p5@1.4.2/lib/p5.js) messo a disposizione dagli sviluppatori di p5.js, che allo stesso modo deve essere richiamata nel documento HTML, come nell'esempio successivo:

```html
<html>
  <head>
    <script src="https://cdn.jsdelivr.net/npm/p5@1.4.2/lib/p5.js"></script>   
  </head>
  <body>
    <main>
    </main>
  </body>
</html>
```

Notare che questa volta il _tag_ `<script>` richiama un link sul web e non un indirizzo locale. In questo modo, è più facile fare affidamento ad una versione sempre pronta della libreria, senza rischio di perdere o corromperne i file localmente, ma se per un qualunque motivo il collegamento non funzionasse allora il nostro progetto risulterebbe rotto.

Quale che sia l'approccio scelto, è sempre utile avere un template di progetto pronto all'uso per poter sperimentare funzionalità della libreria oppure per creare nuovi lavori.

Un altro modo per poter lavorare velocemente con p5.js è tramite l'[editor online](https://editor.p5js.org/) messo a disposizione dagli sviluppatori della libreria. Questo ambiente di prova è perfetto per testare velocemente funzionalità ed semplici idee, ma si riduce solo a questo e qualsiasi progetto più complesso richiede un ambiente di sviluppo in locale aggiornato e funzionante.

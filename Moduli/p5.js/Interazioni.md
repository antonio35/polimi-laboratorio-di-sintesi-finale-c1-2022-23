# Modulo

## Concetti

- Tastiera
- Mouse
- API

## Spiegazione

In **p5.js** per gestire le interazioni da tastiera, sono disponibili un insieme di parametri e funzioni per diversi tipi di azioni. Queste sono

- `keyIsPressed`: Questo parametro è un _flag_ che risulta vero nel momento in cui qualunque tasto risulti premuto, altrimenti è falso.
- `key`: Questo parametro restituisce il valore dell'ultimo tasto premuto. Funziona al meglio solo con i valori **ASCII** o che è possibile premere con un singolo tasto, per caratteri di controllo è meglio utilizzare `keyCode`, mentre per combinazioni multiple (ad esempio caratteri in maiuscolo) è da utilizzare in combinazione con altre funzioni, come `keyTyped()`.
- `keyCode`: Questo parametro restituisce il valore codificato di qualunque tasto, quindi anche dei caratteri di controllo come **SHIFT**, **ALT**, **MAIUSC**, **ESC**, etc.
- `keyIsDown()`: Questa funzione restituisce vero se il carattere impostato come parametro è premuto, altrimenti restituisce falso.
- `keyPressed()`: Questa funzione può essere utilizzata per definire il comportamento che deve avere il programma ogni volta in cui viene premuto un tasto.
- `keyReleased()`: Questa funzione può essere utilizzata per definire il comportamento che deve avere il programma ogni volta in cui viene rilasciato un tasto.
- `keyTyped()`: Questa funzione viene richiamata ogni volta che da sistema viene riconosciuta la pressione di un tasto. Se un tasto viene tenuto costantemente premuto allora la funzione verrà richiamata continuamente fino al suo rilascio.

Nel seguente _sketch_ sono presenti diverse di queste funzioni per gestire dinamicamente un quadrato nel canvas:

```javascript
let canvas = {
  x: window.innerWidth,
  y: window.innerHeight - 10
}

let bg = 255;
let side = 500; 
let corner = 0;
let weight = 1;
let x = canvas.x/2;
let y = canvas.y/2;
let increment = 10;
let flag = false;

function setup() {
  createCanvas(canvas.x, canvas.y);
  stroke(0, 0, 0);
  strokeWeight(weight);
  rectMode(CENTER);
}

function draw() {
  if (keyIsPressed) {    
    if (keyIsDown(87)) { y-=increment; }
    if (keyIsDown(83)) { y+=increment; }
    if (keyIsDown(65)) { x-=increment; }
    if (keyIsDown(68)) { x+=increment; }
  }

  clear();
  strokeWeight(weight);
  fill(bg, bg, bg);
  rect(x, y, side, side, corner);
  
  if (flag) {
    flag = false;
    weight = 1;
    stroke(0, 0, 0);
  }
}

function keyPressed() {  
  switch(key) {
    case ' ':
      bg = 255;
    break;
    case 'Enter':
      bg = 0;
    break;
    default:
    break;
  }
}

function keyReleased() {
  if (
    key === 'w' ||
    key === 'a' ||
    key === 's' ||
    key === 'd'
  ) {
    flag = true;
    weight = 10;
    stroke(255, 0, 0);
  }
}
```

Allo stesso modo, anche le interazioni con il _mouse_ sono ampiamente mappate e semplificate nel seguente insieme di parametri e funzioni:

- `movedX`: Questo parametro è impostato su vero se il _mouse_ è mosso sull'asse _X_, altrimenti è falso.
- `movedY`: Questo parametro è impostato su vero se il _mouse_ è mosso sull'asse _Y_, altrimenti è falso.
- `mouseX`: Questo parametro riporta la posizione del mouse sull'asse _X_ rispetto all'origine di _canvas_.
- `mouseY`: Questo parametro riporta la posizione del mouse sull'asse _Y_ rispetto all'origine di _canvas_.
- `pmouseX`: Questo parametro riporta la posizione del mouse sull'asse _X_ rispetto all'origine di _canvas_ nel _frame_ precedente rispetto a quello attuale. Utile per capire lo scostamento avvenuto nell'ultimo _frame_.
- `pmouseY`: Questo parametro riporta la posizione del mouse sull'asse _Y_ rispetto all'origine di _canvas_ nel _frame_ precedente rispetto a quello attuale. Utile per capire lo scostamento avvenuto nell'ultimo _frame_.
- `mouseIsPressed`: Parametro che viene impostato su vero se un qualunque tasto del _mouse_ risulta premuto, altri è falso.
- `mouseButton`: Parametro che riporta l'ultimo pulsante del mouse premuto.
- `mouseMoved()`: Questa funzione serve ad associare il comportamento da eseguire in caso di movimento del _mouse_.
- `mouseDragged()`: Questa funzione serve ad associare il comportamento da eseguire in caso di contemporanea pressione e movimento del _mouse_.
- `mousePressed()`: Questa funzione serve ad associare il comportamento da eseguire in caso di pressione di un pulsante del _mouse_.
- `mouseReleased()`: Questa funzione serve ad associare il comportamento da eseguire in caso di rilascio di un pulsante del mouse del _mouse_.
- `mouseClicked()`: Questa funzione serve ad associare il comportamento da eseguire in caso di pressione e rilascio di un pulsante del _mouse_ sopra un elemento.
- `doubleClicked()`: Questa funzione serve ad associare il comportamento da eseguire in caso di doppia pressione e rilascio di un pulsante del _mouse_ sopra un elemento.
- `mouseWheel()`: Questa funzione serve ad associare il comportamento da eseguire in caso di scorrimento della rotella del _mouse_.

Ecco un esempio in cui vengono sfruttate molte di queste interazioni per controllare in tempo reale le caratteristiche di un quadrato:

```javascript
let canvas = {
  x: window.innerWidth,
  y: window.innerHeight - 10
}

let bg = 255;
let side = 500; 
let corner = 0;
let weight = 1;

function setup() {
  let cnv = createCanvas(canvas.x, canvas.y);
  cnv.mouseWheel(wheelAction);  
  stroke(0, 0, 0);
  strokeWeight(weight);
  rectMode(CENTER);
}

function draw() {
  clear();
  strokeWeight(weight);
  fill(bg, bg, bg);
  rect(canvas.x/2, canvas.y/2, side, side, corner);
}

function mousePressed() {
  switch (mouseButton) {
    case 'left':
      if (weight > 1) { weight--; }      
    break;
    case 'center':
      if (weight < 20 ) { weight++; }      
    break;
    default:
      console.log(mouseButton);
    break; 
  }
}

function wheelAction(event) {
  if (event.deltaY > 0) {
    if (corner > 1) { corner--; }    
  } else {
    if (corner < 200) { corner++; }
  }
}

function mouseMoved() {
  if (movedY) {
    if (mouseY > pmouseY) {
      if (side > weight + 4) side-=4; }
    else {
      if (side < canvas.x/2 - 4) side+=4;
    }
  }
  if (movedX) {
    if (mouseX > pmouseX) {
      if (bg > 5) bg-=5; }
    else {
      if (bg < 250) bg+=5;
    }
  }
}
```

Possono presentarsi diverse occasioni in cui l'input per un'applicazione non arrivi da interazione utente ne altri dispositivi collegati, ma tramite informazioni ricevute dal web. Ciò è possibile tramite un sistema di **chiamate http** che consentono l'invio di messaggi verso indirizzi che possono restituire risorse o, se presente una **API** (_Application Public Interface_), informazioni dinamiche. È possibile strutturare queste chiamate tramite linguaggio **JavaScript** per un completo controllo della comunicazione, ma p5.js offre delle funzioni per semplificare e snellire questo processo. Una di queste funzioni è `httpGet()`, che permette di effettuare una chiamata **HTTP GET**.

Per funzionare, `httpGet()` deve essere fornito di alcune informazioni:

- `path`: Questo è l'URL a cui effettuare la chiamata da cui ricevere risorse o messaggi.
- `datatype`: Questo rappresenta il tipo di codifica del messaggio di risposta.
- `data`: Questi sono i dati che vengono inviati insieme alla chiamata **GET**.
- `callback`: Questa è la funzione da eseguire in caso di risposta positiva della chiamata.
- `errorCallback`: Questa è la funzione da eseguire in caso di errore nell'esecuzione della chiamata.

Ecco un esempio che, tramite il servizio API offerto dal sito [folgerdigitaltexts](https://www.folgerdigitaltexts.org/api), restituisce il testo de _La Tempesta_ di William Shakespeare e lo mostra su browser.

```javascript
let url = 'https://www.folgerdigitaltexts.org/';
let text = 'Tmp';
let content = 'text';

function setup() {
  let path = `${url}/${text}/${content}`;
  console.log(path);
  httpGet(path, 'text', false, function(response){
    document.querySelector('main').innerHTML = response;
  });
  noLoop();
}
```

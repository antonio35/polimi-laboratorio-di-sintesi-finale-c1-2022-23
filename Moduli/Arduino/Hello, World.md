# Hello, World

## Concetti

- Composizione di uno _sketch_
- LED Blink
- Comunicazione seriale

## Spiegazione

Un programma per **Arduino** editato e caricato con **Arduino IDE**, è scritto in "dialetto" di **C++** realizzato appositamente per venire velocizzare e facilitare i processi della programmazione integrata.

Uno _sketch_ per Arduino è composto da due funzioni chiamate `setup` e `loop`. `setup` viene eseguito una volta sola all'accensione del microcontrollore, ed è qui che vengono impostate le informazioni iniziale del nostro sistema. `loop` invece viene chiamato e ripetuto un numero infinito di volte dopo l'esecuzione di `setup`. Dentro `loop` vive il programma effettivo, dove vengono interpretati gli input, viene aggiornato lo stato del sistema, e vengono attuati gli output. Resettando l'Arduino (variando l'alimentazione, oppure premendo il tasto `Reset` sulla scheda) lo sketch comincerebbe di nuovo da `setup` e poi continuerebbe a ciclare dentro `loop`.

Accendere un _LED_ (e non bruciarlo) è il progetto più semplice ma comunque altamente significativo che può essere fatto in programmazione integrata. È di fatto il primissimo test per provare per controllare il funzionamento di un microcontrollore.

L'integrazione di questo componente è molto lineare, oltre al _LED_ stesso è necessario solo un resistore con la giusta quantità di Ohm (per una corrente da 5v, va benissimo un resistore fra i 200-500Ω), da mettere indifferentemente primo o dopo il _LED_. È importante ricordare che il _LED_ ha un orientamento e che il catodo deve andare verso la messa a terra, l'anodo invece deve essere collegato ad un qualunque _pin_ di output del microcontrollore.

![Blink circuit](../../Media/arduino__hello__blink.png)

Il seguente _sketch_ serve a testare il circuito, alternando accesione e spegnimento del _LED_ con un intervallo di 500 millisecondi:

```c++
#define LED 11

void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
}
```

La prima istruzione è una **macro**, e serve a dire durante la compilazione tutte le volte che nel codice sorgente appare l'etichetta `LED` questa dovrà essere sostituita con il valore definito nella macro (in questo caso `11`).

Dentro il blocco di `setup`, la funzione `pinMode()` serve a definire l'utilizzo del _pin_ espresso come primo parametro, in questo caso il _pin_ `11` è definito come `OUTPUT` (questa operazione è necessaria per alcune impostazioni interni del microcontrollore, e quindi rendere il sistema più affidabile).

Dentro `loop`, invece le funzione `digitalWrite()` serve ad impostare il valore di un _pin_. Nel nostro caso variano fra `HIGH` e `LOW`, per poter accendere e spegnere il _LED_. la funzione `delay()` invece serve a fermare l'esecuzione del programma secondo il parametro passato in millisecondi (in questo caso `500`).

Un altro esempio molto pratico per testare il funzionamento di un Arduino è quello di mandare un messaggio tramite le funzioni della comunicazione seriale. Non c'è bisogno di integrare alcun componente, basta aver collegato un cavo USB fra Arduino e computer con Arduino IDE, ed aprire il **Monitor Seriale** di quest'ultimo tramite il menù `Tools -> Serial Monitor`.

Ecco un esempio di codice:

```c++
void setup() {
  Serial.begin(9600);
  Serial.println("Hello, World!");
}

void loop() {
  //
}
```

Dentro il blocco di `setup`, la funzione `Serial.begin()` serve a stabilire la comunicazione seriale con il PC ad una velocità di `9600` baudrate (questa impostazione andrà anche scelta nel monitor seriale dell'_IDE_). La funzione `Serial.println()` serve invece a inviare una stringa di caratteri come messaggio.

![Hello Serial](../../Media/arduino__hello__serial.png)
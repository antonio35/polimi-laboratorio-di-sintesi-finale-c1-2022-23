# Attuatori

## Concetti

- Attuazione
- Digitale
- Analogico
- Transistor
- Driver

## Spiegazione

Gli attuatori sono i componenti che permettono ad un microcontrollore di comunicare con il mondo esterno tramite infomrazioni che possono essere interpretate sia da utenti che da altri macchine. Queste informazioni sono trasmesse tramite canali sensoriali come la luce o il suono, ma anche cinetici tramite motori, oppure tramite fenomeni come il magnetismo o il calore. In ogni caso, è sempre importante avere un pieno controllo del proprio _medium_ di attuazione, per due motivi:

1. Per poter attuare e trasmettere informazioni in maniera disambigua, chiara ed interpretabile
2. Per evitare problemi legati ad un uso non sicuro degli attuatori. Di fatto, solitamente gli attuatori richiedono molta corrente - e quindi maggiori rischi - e possono creare situazioni di pericolo - tramite movimenti, reazioni e conseguenze - sia per gli utenti, per l'ambiente di lavoro, che per lo stesso circuito.

Per questi motivi, questo ambito è sicuramente uno di quelli in cui vale la pena di ricordare la celebre massima: _**R.T.F.M.**_.

Abbiamo già visto come accendere un _LED_ nel modulo [Hello World](Hello%2C%20World.md). Ecco di seguito lo stesso circuito utilizzando un _pin_ differente.

![output digital](../../Media/arduino__output__digital.png)

```c++
int led 3
int t = 500;

void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  digitalWrite(led, HIGH);
  delay(t);
  digitalWrite(led, LOW);
  delay(t);
}
```

Come abbiamo visto, la funzione `digitalWrite()` è quella che permetteo di "accendere" e "spegnere" i _pin_ programmaticamente, ed è il modo per utilizzarli in maniera digitale.

Vale la pena spendere alcune parole in più sull'integrazione del _LED_ nel nostro circuito. Un _LED_ è un componente _non-ohmico_, cioè non offre resistenza al passaggio della corrente elettrica sebbene ne assorba per emettre luce. Per questo motivo, è necessario aggiungere un resistore per permettere che il LED non bruci, facendo in modo che la corrente arrivi "meno forte" al _LED_. Ogni resistore ha un valore di resistenza espresso in **Ohm** (o **Ω**) e la quantità giusta per questo tipo di circuito è da valutare secondo un'estensione della **Legge di Ohm**.

La legge di Ohm dice che la tensione è uguale alla corrente per la resistenza: **V = I * R**. Del nostro circuito, noi abbiamo già ben chiari cosa sono _V_ ed _I_. Infatti, _V_ è espresso dall'alimentazione dell'Arduino, ed è _5v_, mentre _I_ è la corrente assorbita dal _LED_ sotto tensione, che può essere consultata dal suo _datasheet_. Solitamente, un _LED_ per questo tipo di circuiti assorbe _20mA_.

Con questi valori, è dunque possibile ribaltare la legge di Ohm per ricavare la resistenza ideale del circuito: **R = V / I**. Cioè `R = 5 / 0,02 = 250 Ω`.

Questo valore, però, non è ideale, perchè il _LED_ possiede in realtà un altro valore leggibile su _datasheet_ che è la tensione inversa (_reverse voltage_) da sottrarre alla tensione nominale, secondo la seguente formula: **R = (Vn - Vr) / I**. In un comune _LED_ questo valore è circa _3v_.

In questo modo se ne ricava che il valore di resistenza più efficace è `R = (5 - 3) / 0,02 = 100 Ω`. Scegliendo un resistore calcolato in questo modo otterremo la migliore efficenza disponibile per il nostro _LED_ (e quindi la maggiore luminosità).

Seguendo questo ragionamento, è possibile creare circuiti più complessi e più efficienti, sopratutto per applicazioni analogiche di attuazione, come il seguente esempio in cui usiamo un _LED RGB_.

![output analog](../../Media/arduino__output__analog.png)

Studiando il _datasheet_ di questo componenete è possibile scoprire l'assorbimento di corrente e la tensione inversa di ogni singolo canale di colore, e quindi calcolare la corretta resistenza da applicare (in modo da avere una gestione lineare e coordinata di tutti i colori).

Da notare che i _pin_ a cui è collegato il _LED_ sono contrassegnati da una tilde (**~**). Questa serve ad identificare i piedini **PWM** (_Pulse Width Modulation_) del controllore, cioè quelli che possono simulare un output analogico.

Di seguito, uno _sketch_ per il circuito di sopra che testa in maniera circolare tutti i colori:

```c++
#include <math.h>

int ledR = 5;
int ledG = 3;
int ledB = 5;

void setup(){
  pinMode(ledR, OUTPUT);
  pinMode(ledG, OUTPUT);
  pinMode(ledB, OUTPUT);
}

void loop(){
  for (short i = 0; i < 255; i++  ) {    
    int r = map(i, 0, 255, 0, M_PI * 2);
    analogWrite(ledR, abs(sin(r)) * 255);
    analogWrite(ledG, abs(sin(r + (M_PI * 2 * 0.33))) * 255);
    analogWrite(ledB, abs(sin(r + (M_PI * 2 * 0.66))) * 255);
  }
}
```

Per far funzionare correttamente questo _sketch_ è necessario aggiungere la libreria _standard_ `math.h`, questo perchè faremo uso nel codice di alcune funzioni matematiche.

Dentro `setup()` vengon impostati i _pin_ come `OUTPUT`. Invece all'interno di `loop()` viene dichiarato un ciclo nel quale vengono impostati in _pin_ PWM tramite la funzione `analogWrite()`. Similmente all'equivalente digitale, in questa funzione viene dichiarato il _pin_ da impostare ed il valore numerico da assegnargli. Questo numero può variare da _0_ a _255_, in quanto la risoluzione del PWM è di 8 bit.

Dentro il ciclo, tramite la funzione trigonometrica `sin()` viene assegnato un valore periodico dentro il dominio _0-255_, ed i vari canali di colore sono sfalsati fra di loro di _120°_, per ottenere un risultato più equilibrato ed armonico.

In certi casi, un attuatore può richiedere un assorbimento elettrico maggiore rispetto a quello che può un microcontrollore sui suoi piedini (tenere a mente che un Arduino può fornire complessivamente al massimo _200mA_ di corrente, e comunque non più di _50mA_ su un singolo _pin_), oppure funzionare con livelli di tensioni differenti. Sorpassare certi limiti può comportare la bruciatura (alla lettera) del microcontrollore, oppure rendere inoperativo l'attuatore, e quindi è il caso di integrare un'alimentazione esterna che possa fornire la corrente necessaria.

Il modo più semplice di gestire correnti di livelli differenti tramite un microcontrollore è con l'integrazione di un **transistor**, un componente elettronico che permette di aprire o chiudere il passaggio di corrente tramite l'attivazione di un _pin_ dedicato.

Per esempio, un circuito per attivare uno speaker può essere costruito tramite un transistor **NPN**, come nel seguente schema.

![output library](../../Media/arduino__output__library.png)

In questo circuito il _pin 2_, quando alimentato, permette di aprire il passaggio della corrente nel transistor, in questo modo alimentando lo _speaker_ con la batteria da _9V_. La gestione del _pin_ consente quindi di creare delle forme d'onda che possono generare dei segnali acustici. Questo funzionamento viene semplificato dalla funzione `tone()` della libreria di Arduino che consente di generare delle onde quadre per creare dei toni.

```c++
#include "pitches.h"
int lowNote = NOTE_G3;
int highNote = NOTE_C4;
int noteDuration = 1000;
int pause = 1500;
int speaker = 3;

void setup() {
  pinMode(speaker, OUTPUT);  
}

void loop() {
  for (short i = 0; i < 3; i++) {
    tone(speaker, lowNote, noteDuration);
    tone(speaker, highNote, noteDuration);
    noTone(speaker);
    delay(pause);
  }
}
```

L'inclusione di `pitches.h` serve a richiamare delle frequenze costanti per i toni. Dentro la parte di loop invece vengono chiamati a ripetizione due toni grazie alla funzione ´tone()´, nella quale il primo paramentro è il _pin_ da attivare, il secondo è la frequenza da emettere, ed il terzo è la durata da generare. La successiva funzione `noTone()` serve a interrompere il tono (fondamentale se `tone()` deve essere usato anche su altri canali).

Nonostante i _transistor_ siano dei componenti molto semplici, tramite configurazioni particolari è possibile ottenere dei meccanismi sofisticati ed utili a pilotare attuatori più complessi (chiamati infatti **driver**). È il caso, per esempio, del **ponte H**, una configurazione di _transistor_ che in maniera semplice e diretta permette di guidare un **motore DC** in entrambi i sensi di marcia (un motore DC, infatti, è un attuatore semplice ma impratico, che senza accorgimenti particolari può ruotare in una sola direzione senza capacità di controllo). Nel diagramma di seguito, il ponte H è racchiuso dentro un componente integrato chiamato **L293D** e viene mostrato come poterlo collegare fra un Arduino, una batteria da _9v_ ed un motore.

![output driver](../../Media/arduino__output__driver.png)

Un ponte H per funzionare ha bisogno di 3 _pin_: uno per attivare il circuito (_7_ nel nostro schema), uno per aprire il canale destro (_5_), ed uno per aprire il canale sinistro (_6_). Per muovere il motore in una direzione, il relativo canale deve essere accesso, mentre l'altro deve essere spento. Se entrambi i canali sono aperti allora il motore è in blocco, se entrambi sono spenti allora è in stato di riposo.

Un tipico _sketch_ per sfruttare questa configurazione è il seguente.

```c++
int enable = 7;
int left = 6;
int right = 5;

void setup() {
  pinMode(enable, OUTPUT);
  pinMode(left, OUTPUT);
  pinMode(right, OUTPUT);
  digitalWrite(enable, HIGH);
}

void loop() {
  digitalWrite(right, LOW);
  for (short i = 0; i < 250; i += 50) {
    analogWrite(left, i);
    delay(500);
  }
  digitalWrite(left, LOW);
  delay(500);
  for (short i = 0; i < 250; i += 50) {
    analogWrite(right, i);
    delay(500);
  }
  digitalWrite(right, LOW);
  delay(500);
}
```

Nell'esempio di sopra, le proprietà del ponte H vengono utilizzate dentro `loop()`, tramite dei cicli che a scatti di _500_ millisecondi aumentano la velocità del motore, prima in un verso e poi nell'altro.

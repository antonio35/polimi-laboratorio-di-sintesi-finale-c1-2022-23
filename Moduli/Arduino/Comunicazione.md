# Modulo

## Concetti

- Comunicazione seriale
- `Serial`
- Inviare
- Ricevere
- Interpretare

## Spiegazione

Un microcontrollore come l'**Atmega328** offre molte interfacce di comunicazione a livello _hardware_, come _I2C_, _SPI_ e _USART_, e queste possono essere gestite sia tramite un'implementazione diretta del protocollo di comunicazione (andando ad agire in maniera codificata e temporizzata sui _pin_), oppure tramite l'utilizzo di librerie che facilitano enormemente questo compito con istruzioni semplici e facili da leggere.

**Arduino** semplifica ulteriormente queste funzionalità tramite l'integrazione di componenti che permettono di comunicare rapidamente, e senza ulteriori interfacce, tramite l'**USB** saldata sulla scheda. Dall'altro lato del cavo, un computer con un programma di lettura dei messaggi su porta seriale (come **Serial Monitor**, integrato dentro **Arduino IDE**) può facilmente leggere ed inviare messaggi con la scheda.

La libreria di Arduino offre una classe di funzioni molto avanzate e pratiche per la comunicazione seriale chiamata `Serial`. Per instanziare questa classe, è necessario aggiungere un'istruzione nel primo blocco di codice, come nella maniera seguente:

```c++
void setu() {
  Serial.begin(9600);
}
```

In questo modo viene stabilita propriamente la comunicazione con la porta seriale. Il numero usato dentro l'istruzione è il **baud rate**, cioè la velocità con cui vengono inviati messaggi nel canale (espressi come _simboli per secondo_). _9600_ è un valore tipico ed affidabile per la velocità di computazione di un Arduino, ma per casi particolari è possibile utilizzare valori maggiori.

Una volta stabilita la comunicazione è possibile mandare messaggi sulla porta USB tramite le seguenti funzioni:

```c++
Serial.print("This is a message");
Serial.println("This is a message with NL & CR");
```

La funzione `Serial.print()` invia una stringa senza aggiungere niente in conclusione al messaggio. `Serial.println()` invece invia la stringa ed alla fine del messaggio aggiunge due caratteri di controllor, **CR** e **NL**, per poter tornare indietro ed a capo, in questo modo permettedo di scrivere su più righe. È importante sapere che entrambe le funzioni possono inviare anche messaggi contententi altri tipi di variabili oltre a stringhe.

Dall'altra capo nella comunicazione seriale, è importante che il programma per leggere la porta seriale sia impostato allo stesso _baud rate_ (altrimenti, il risultato sarà impossibile da decifrare).

Se invece si desidera leggere un messaggio in arrivo tramite comunicazione seriale, allora è necessario impostare alcune istruzioni per evitare trasmissioni corrotte ed inaffidabili, come nel seguente modo:

```c++
String message = '';

void setup() {
  Serial.begin(9600);
}

void loop() {
  while (Serial.available()) {
    message = Serial.read() + message;
  }
}
```

In questo _sketch_, dopo aver impostato la comunicazione, dentro `loop()` viene creato un ciclo basato sul risultato della funzione `Serial.available()`. Questa istruzione restituisce il numero di `byte` presente nel _buffer_ della porta seriale, cioè controlla che ci sia un messaggio in arrivo da interpretare. Se questo è il caso allora tramite l'istruzione `Serial.read()` viene preso il primo _byte_ nel _buffer_ e viene aggiunto alla variabile `message`. Alla conclusione ciclo la variabile avrà al suo interno il messaggio completo.

I messaggi che vengono ricevuti tramite comunicazone seriale sono sempre caratteri codificati in **ASCII**, anche se questi rappresentano numeri, e se il messaggio ricevuto deve essere utilizzato per delle operazioni è necessario saperlo manipolare per poter estrapolare l'informazione che ci interessa. Questa operazione in informatica si chiama _parsing_.

Un sistema per convertire un numero ricevuto tramite comunicazione seriale è il seguente:

```c++
char message = '';
int value;
int magnitude;

void setup() {
  Serial.begin(9600);
}

void loop() {
  if (Serial.available) {
    value = 0;
    magnitude = 0;
    while (Serial.available()) {
      message = Serial.read();
      value = (message - 48) * (10 ^ magnitude);
      magnitude++;
    }
    Serial.println(value);
  }  
}
```

Analizzando la parte di `loop()`, si può notare come il programma controlli la presenza di messaggi nella porta seriale. Se questo è presente allora procede a leggere ogni carattare in ingresso, ma siccome vogliamo intenpretarli come numeri allora è necessario sottrarre `48` ad ogni lettura. Questo perchè nella codifica ASCII in numeri arabi cominciano dalla quarantottesima posizione a partire da `0`e quindi, per convertirli da `char` a `int`, devo manipolarli in questa maniera. Inoltre, tramite la variabile `magnitude`, che incrementa ad ogni ciclo, faccio in modo che ogni numero sia rapportato al suo ordine di grandezza. In conclusione, stampo nuovamente il valore per controllare che la lettura sia stata corretta.

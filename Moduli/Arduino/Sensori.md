# Sensori

## Concetti

- Rilevazione
- Digitale
- Analogico
- Librerie

## Spiegazione

I sensori sono il modo in cui un microcontrollore può ricevere informazioni dal suo ambiente, tramite l'attivazione esterna dei suoi _pin_. Queste rilevazioni possono essere un'azione diretta dell'utente (come nel caso della pressione di pulsanti), un meccanismo operato da altri macchinari (per esempio con gli interruttori di fine corsa), oppure ancora una lettura ambientale di informazioni quantificabili (come per esempio in rilevatori di gas, oppure in celle di carico).

Per via di questi molti casi, e delle molteplici ramificazioni sottostanti ad ognuno, esistono tantissimi sensori per ogni tipo di applicazione, spesso differendo fra di loro per dettagli minimi ma comunque fondamentali (per esempio, il _range_ di frequenze leggibili di un microfono), e spesso non propriamente legati alla misurazione ma alla sua operatività (per esempio, l'altezza di un meccanismo di attuazione di un pulsante può essere di molte altezze, ma magari solo una è adatta al proprio specifico progetto). Per questo, per scegliere il giusto sensore è fondamentale rifarsi al vecchio adagio: _**R.T.F.M.**_.

Il meccanismo più semplice per inviare un'informazione ad un microcontrollore e per gestirne il suo stato è tramite un pulsante. Solitamente, in elettronica quando si parla di pulsanti si intendono gli interruttori tattili momentanei, cioè quei pulsanti con internamente una mollta che si attivano con la pressione e tornano in stato di riposo al rilascio. Questo sistema prettamente digitale può essere costruito con il seguente schema.

![input digital](../../Media/arduino__input__digital.png)

Il sistema di integrazione usato per questo interruttore si chiama **resistenza di pull-up**. Il suo compito è quello di stabilizzare la corrente sul piedino che verrà utilizzato per la lettura dell'interruttore aggiungendo una resistenza di valore alto collegata all'alimentazione (in questo caso, da _10KΩ_). Questo è fondamentale perchè per via di molti fattori la corrente sul piedino può subire delle fluttuazioni anche particolarmente ampie che possono andare a craere dei falsi positivi nelle nostre rivelazioni. La resistenza di _pull-up_ fa in modo che la corrente sul piedino sia sempre alta in maniera stabile, facendo sì che quando il pulsante viene premuto il _pin_ possa chiaramente e senza ambiguità leggere uno stato differente. L'attenuazione creata dal resistore fa in modo che non si creino corti circuiti fra alimentazione e messa a terra. Lo stesso stratagemma viene utilizzato anche in altri sensori, come gli **FSR**, oppure i **fotoresistori**.

Ecco uno _sketch_ per la rilevazione della pressione del pulsante.

```c++
int button = A0;
bool flag;

void setup() {
  Serial.println(9600);
  pinMode(button, INPUT);
  flag = false;
}

void loop() {
  if (digitalRead(button) == LOW) {
    if (flag == false) {
      Serial.println("Pulsante premuto");
      flag = true;
    }
  } else {
    if (flag == true) {
      flag = false;
    }
  }
}
```

La funzione su cui si basa tutto lo _sketch_ è `digitalRead()`, che passando un numero di _pin_ come parametro restituisce il suo stato come un valore booleano. Ciò che vogliamo ottenere è che alla pressione del pulsante venga trasmesso tramite porta seriale un messaggio. Notare che in questo esempio, controlliano il _pin A0_ per valori negativi, questo perchè grazie alla resistenza di pull-up su quel piedino la corrente è sempre presente.

Nel codice è anche presente un semplice stratagemma per consentire l'invio del messaggio di pressione solo nel momento in cui questa effettivamente avviene. Grazie alla variabile `flag` teniamo traccia dello stato del _pin A0_ e possiamo verificare il passaggio di valore grazie alla condizione appena dopo la lettura del sensore. Con l'avvenuto passaggio anche `flag` si aggiorna, in modo da non ripetere l'invio del messaggio fintanto che il pulsante è premuto. Quindi, una volta rilasciato il pulsante, `flag` viene riportato allo stato iniziale. In questo modo evitiamo una mole di segnalazioni non utili e possibilmente confusioarie.

Il gruppo di _pin_ di un Arduino preceduto dalla lettera **A** possiede al suo interno un **ADC** (_Analog-to-Digital-Converter_) e permette dunque la lettura di valori analogici. Con questo si intende che invece di restituire un valore booleano, questi piedini permettono di leggere 1024 gradi di differenziazione della tensione (questo perché l'ADC ha una risoluzione di _10 bit_). Ecco uno schematico che sfrutta questa proprietà tramite l'utilizzo di un potenziometro.

![input analog](../../Media/arduino__input__analog.png)

Un potenziometro è un sensore che permette di restituire una tensione variabile a seconda della posizione del suo attuatore (la ghiera mobile sopra di esso) consentendo così un controllo continuo e discreto per l'utente.

Ecco uno _sketch_ per che restituisce i valori del potenziometro tramite comunicazione seriale:

```c++
int pot = A0;
int previous;

void setup() {
  Serial.begin(9600);
  pinMode(pot, INPUT);
  previous = analogRead(pot);
  Serial.print("Potenziometro: ");
  Serial.println(previous);
}

void loop() {
  int tmp = analogRead(pot);
  if(previous != tmp) {
    Serial.print("Potenziometro: ");
    Serial.println(tmp);
    previous = tmp;
  }
}
```

In questo caso, la funzione principale è `analogRead()`, che restituisce un valore da _0_ a _1023_ a seconda della tensione sul _pin_ che le viene passato.

Anche in questo esempio abbiamo sfruttato uno stratagemma simile a quello precedente con `flag`, ma tramite il salvataggio un valore intero invece che di un booleano. In questo modo, al variare del potenziometro viene inviato un messaggio di aggiornamento, altrimenti non succede niente.

Al giorno d'oggi è possibile trovare sensori in molte configurazione e con proprietà raffinate ed avanzate, inclusi in circuiti integrati pronti per essere collegati ed utilizzati in pochi minuti e righe di codice, grazie all'utilizzo di librerie più o meno standardizzato da industria, venditori o comunità. In questo modo è possibile velocizzare il proprio lavoro e non dover inventare ogni volta la ruota, ma vale la pena ricordare che molti sistemi di rilevazione, se ridotti all'osso, possono essere facilmente ricreati con semplici stratagemmi.

Uno dei casi più esemplari è quello dei sensori capacitivi, componenti che riescono a rilevare il tocco da parte dell'utente (anche in maniera analogica) e che sono altamente industrializzati, al punto di essere integrati in moltissimi dispositivi quotidiani (per esempio, negli schermi dei telefoni). Creare un sensore capacitivo da zero in realtà è abbastanza semplice, ed il suo utilizzo con un microcontrollore può essere facilitato tramite una libreria esterna.

Di seguito ecco un esempio di sensore capacitivo.

![input touch](../../Media/arduino__input__touch.png)

Si può notare che ci sono ben pochi componenti in questo sistema, ciò nonostante questo è quanto basta. Di fatto tutto quello che serve è un resistore ad alto valore (in questo caso _1MΩ_) a creare una messa a terra virtuale fra due _pin_, ed un collegamento ad uno dei due che servirà per l'interazione dell'utente (solitamente la superficie di questo collegamento viene ampliata con altro materiale conduttivo tipo una barra di metallo oppure della pellicola di alluminio). Se è tutto ben collegato e funzionante, il collegamento riuscirà a rilevare una differenza capacitiva che si manifesta quando il filo viene toccato da un utente.

Ecco il codice per farlo funzionare.

```c++
#include <CapacitiveSensor.h>

#define SAMPLE 30
#define SEND 12
#define RECEIVE 6
#define DELTA 200

CapacitiveSensor cs = CapacitiveSensor(SEND, RECEIVE);
unsigned long timer;
long sensing;

void setup() {
  Serial.begin(9600);
  timer = millis();
}

void loop() {
  sensing = cs.capacitiveSensor(SAMPLE);
  if (millis() - timer > DELTA) {
    Serial.println(sensing); 
    timer = millis();   
  }
}
```

In questo esempio, `CapacitiveSensor.h` è la libreria che ci facilità le operazioni legate al sensore capacitivo, in quanto ci fornisce la classe `CapacitiveSensor` adatta a questo scopo. Una volta inizializzata con i due pin di riferimento all'interno di `loop()` utilizziamo un sistema per inviare tramite messaggio il valore rilevato dal sensore a cadenza temporale fissa, tramite la funzione `capacitiveSensor()` dell'oggetto instanziato in precedenza. Per esempio, avvicinando ed allontanando la mano da questo sistema sarà possibile vedere che il valore differirà ampiamente, fornendo così un sistema di interazione chiaro ed affidabile.
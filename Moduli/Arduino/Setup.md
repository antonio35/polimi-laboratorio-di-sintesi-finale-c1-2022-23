# Setup

## Concetti

- Arduino IDE
- Collegamento
- Caricamento
- Struttura progetto
- Arduino CLI

## Spiegazione

È possibile lavorare con una scheda **Arduino** scaricando la _IDE_ dal sito ufficiale [Arduino.cc](https://www.arduino.cc).

Una volta installato, la _IDE_ offre un editor di testo per scrivere il codice, e diverse funzionalità che permettono di testare, adattare e caricare il codice dentro un Arduino.

![Arduino IDE](../../Media/arduino__setup__ide.png)

Per prima cosa, l'Arduino deve essere collegato tramite USB al computer. Quindi, una volta preparato il codice, è necessario impostare la scheda di destinazione dal menù `Tools -> Board`, e la porta su cui la cui la scheda è collegata tramite il menù `Tools -> Port`.

![Arduino IDE](../../Media/arduino__setup__board.png)

![Arduino IDE](../../Media/arduino__setup__port.png)

È possibile anche salvare i progetti, per poterci lavorare in sessioni differenti. Arduino IDE lavora con file di estensione `.ino` (detti anche _sketch_), che devono essere posizionati in cartelle con lo stesso nome del file. All'interno della cartella di uno _sketch_ possono essere presenti anche ulteriori file da includere nel codice sorgente, come classi personalizzate, librerie esterne, e file dati.

Per progetti che richiedono maggiore flessibilità, personalizzazione, e performance, è possibile eseguire tutte queste operazioni anche tramite **Arduino CLI**, il toolkit per poter programmare schede Arduino (e molto altro) direttamente da riga di comando. Una tipica istruzione per caricare un programma è la seguente:

```bash
arduino-cli upload SketchFolder -b arduino:avr:uno -p COM3
```

Tramite questo comando, stiamo richiedendo programmare una scheda **Arduino UNO** con il _flag_ `-b`, che questo è connesso nella porta `COM3` con il _flag_ `-p`, e che il programma da caricare si trova nella cartella `SketchFolder`.  
